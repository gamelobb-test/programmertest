﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimalSpawnner : MonoBehaviour
{
    public GameObject animalToSpawn;

    private float interval = 1.5f;

    private float timeElapsed = 0;

    List<GameObject> animalList;

    private void Start()
    {
        animalList = GameManager.instance.animalList;
    }

    // Update is called once per frame
    void Update()
    {
        timeElapsed += Time.deltaTime;

        if (timeElapsed >= interval)
        {
            var ramdomValueX = Random.Range(-2f, 2f);

            var animal = Instantiate(animalToSpawn, new Vector2(ramdomValueX, transform.position.y),Quaternion.identity);

            animalList.Add(animal);

            timeElapsed = 0;
        }
    }
}
