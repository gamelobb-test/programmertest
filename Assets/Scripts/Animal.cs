﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Animal : MonoBehaviour,IPointerClickHandler
{
    private int score = 1;

    public void OnPointerClick(PointerEventData eventData)
    {
        GameManager.instance.PlayerScore += score;
        Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Finish")
        {
            GameManager.instance.GameOver = true;
            Destroy(gameObject);
        }
    }
    private void OnDestroy()
    {
        GameManager.instance.animalList.Remove(gameObject);
    }
}
